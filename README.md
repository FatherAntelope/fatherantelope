<h2> Frontend-developer </h2>

<h3> Hard skills </h3>

> <img src="https://img.shields.io/badge/-JavaScript-black?style=for-the-badge&logo=javascript"/>
> <img src="https://img.shields.io/badge/-TypeScript-black?style=for-the-badge&logo=typescript"/>
> <img src="https://img.shields.io/badge/-HTML-black?style=for-the-badge&logo=HTML5"/>
> <img src="https://img.shields.io/badge/-CSS-black?style=for-the-badge&logo=CSS3"/>
> <img src="https://img.shields.io/badge/-SCSS-black?style=for-the-badge&logo=SASS"/>
> <img src="https://img.shields.io/badge/-GIT-black?style=for-the-badge&logo=git"/>

<h3> Other technologies </h3>

> <img src="https://img.shields.io/badge/-React-black?style=for-the-badge&logo=react"/>
> <img src="https://img.shields.io/badge/-Redux-black?style=for-the-badge&logo=redux"/>
> <img src="https://img.shields.io/badge/-PHP-black?style=for-the-badge&logo=php"/>
> <img src="https://img.shields.io/badge/-Bootstrap-black?style=for-the-badge&logo=Bootstrap&logoColor=563D7C"/>
<!--<img src="https://img.shields.io/badge/-C%23-black?style=for-the-badge&logo=C-Sharp&logoColor=47C5FB"/>-->
<!--<img src="https://img.shields.io/badge/-C++-black?style=for-the-badge&logo=C%2b%2b"/>-->

<h3> Follow me </h3>

> <a href="https://vk.com/vladlengorbunov"><img src="https://img.shields.io/badge/-Вконтакте-black?style=for-the-badge&logo=VK"/></a>
> <a href="https://www.instagram.com/father_antelope/"><img src="https://img.shields.io/badge/-Instagram-black?style=for-the-badge&logo=Instagram"/></a>
> <a href="https://www.codewars.com/users/FatherAntelope"><img src="https://img.shields.io/badge/-CodeWars-black?style=for-the-badge&logo=CodeWars"/></a>

<h3> Statistics </h3>
<p>
<img src="https://github-readme-stats.vercel.app/api?username=fatherantelope&show_icons=true&theme=dracula&border_radius=0?hide_border=true&bg_color=303030&border_color=303030&text_color=00a634&icon_color=bf3d3d&title_color=654ed9"/>
<img src="https://github-readme-stats.vercel.app/api/top-langs/?username=fatherantelope&theme=dracula&layout=compact&border_radius=0&bg_color=303030&border_color=303030&text_color=00a634&title_color=654ed9"/>
<img
  src="https://cr-skills-chart-widget.azurewebsites.net/api/api?username=fatherantelope&branding=false&skills=JavaScript,PHP,TypeScript,React,CSS,SCSS,HTML&bg=%23303030&padding=10&height=200&width=800"
/>
 
</p>

<h3> Education </h3>
<img
  src="https://cr-ss-service.azurewebsites.net/api/ScreenShot?widget=education&username=fatherantelope&max-items=4&branding=false&grid=true&style=--item-bg-color:%23303030;--item-padding:10px;--title-text-color:%235b49b6;--details-text-color:%23fff;--date-text-color:%2302a43b;--date-font-size:12px;--preloader-color:%2302a43b;--grid-columns:2"
/>
